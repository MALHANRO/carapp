﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using CarLibrary;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CarUwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private CarRepository _carRepository = new CarRepository();
        public MainPage()
        {
            this.InitializeComponent();
        }
        //private void OnPageLoad(object sender, RoutedEventArgs e)
        //{
        //   ImgUsersChoice.Loaded=false;
        //}

        private Car CaptureCarInfo()
        {
        //   var _enumVal = Enum.GetValues(typeof(CarType)).Cast<CarType>().ToList<CarType>();
           // var typecol = new GridComboBoxColumn();  
            string vinNumber = TxtCarVin.Text;
            string carMake = TxtCarMake.Text;
            float purchasePrice = float.Parse(TxtPurchasePrice.Text);
           // CarType carType = (CarType)CmbCarType.SelectedItem;
            int modelYear = int.Parse(CmbModelYear.SelectedItem.ToString());
            int mileage = int.Parse(TxtMileage.Text);
          
            CarType carType = (CarType)CmbCarType.SelectedItem;
          
        //    CmbCarType.ItemsSource = _enumVal.ToList<CarType>();
            Car c = new Car(vinNumber, carMake, purchasePrice, carType, modelYear, mileage);
          //  Car c = new Car(vinNumber, carMake, purchasePrice, carType, modelYear, mileage,depreceationAmount);
            return c;
        }



        public IList<CarType> CarTypes
        {
            get { return Enum.GetValues(typeof(CarType)).Cast<CarType>().ToList<CarType>(); }
        }
        public CarType CarType
        {
            get;
            set;
        }

        private void RenderCarInfo(Car car)
        {
            TxtCarVin.Text = car.VinNumber;
            TxtCarMake.Text = car.CarMake;
            TxtPurchasePrice.Text = car.PurchasePrice.ToString();
            CmbCarType.Text = car.CarTypeN.ToString();
            CmbModelYear.Text = car.ModelYear.ToString();
            TxtMileage.Text = car.Mileage.ToString();
        }

        private void ClearUI()
        {
            TxtCarVin.Text = " ";
            TxtCarMake.Text = " ";
            TxtPurchasePrice.Text = " ";
            CmbCarType.Text = " ";
            CmbModelYear.Text = " ";
            TxtMileage.Text = " ";
            TxtErrorMessage.Text = " ";
        }

        private void OnAddCar(object sender, RoutedEventArgs e)
        {
            try
            {
                Car c = CaptureCarInfo();
                _carRepository.AddCar(c);
                LstCars.Items.Add(c);
                
            }
            catch (Exception error)
            {
                TxtErrorMessage.Text = error.Message;
            }
        }
     
        private void OnClear(object sender, RoutedEventArgs e)
        {
            ClearUI();
        }

        private void OnCarSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Car c = (Car)LstCars.SelectedItem;
            RenderCarInfo(c);
        }
        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        
            if (CmbCarType.SelectedIndex == 0)
            {
                ImgUsersChoice.Source = new BitmapImage
                    (new Uri($"ms-appx:///Assets/CarImages/VinNumber01.jpg"));
            }
            else if (CmbCarType.SelectedIndex == 1)
            {
                ImgUsersChoice.Source = new BitmapImage
                           (new Uri($"ms-appx:///Assets/CarImages/VinNumber02.jpg"));
            }
            else if (CmbCarType.SelectedIndex == 2)
            {
                ImgUsersChoice.Source = new BitmapImage
                           (new Uri($"ms-appx:///Assets/CarImages/VinNumber03.jpg"));
            }
            else if (CmbCarType.SelectedIndex == 3)
            {
                ImgUsersChoice.Source = new BitmapImage
                           (new Uri($"ms-appx:///Assets/CarImages/VinNumber04.jpg"));
            }
            else if (CmbCarType.SelectedIndex == 4)
            {
                ImgUsersChoice.Source = new BitmapImage
                           (new Uri($"ms-appx:///Assets/CarImages/VinNumber05.jpg"));
            }
            else
            {
                TxtErrorMessage.Text = "Select From the List above";
            }
        }
    }
}
