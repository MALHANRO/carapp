﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLibrary
{
   public class Car
    {
        private string _vinNumber;
        private string _carMake;
        private float _purchasePrice;
        private CarType _carType;
        private int _modelYear;
        private int _mileage;
        private float _depreciationAmount;

        public override string ToString()
        {
            return $"{VinNumber},{CarMake},{ModelYear},{Mileage},{TotalDepreciation}";
        }

        public Car (string vinNumber, string carMake, float purchasePrice, CarType carType, int modelYear, int mileage)
        {
            this.VinNumber = vinNumber;
            CarMake = carMake;
            PurchasePrice = purchasePrice;
            CarTypeN = carType;
            ModelYear = modelYear;
            Mileage = mileage;
           // TotalDepreciation = depreciationAmount;
        }

        public string VinNumber
        {
            get
            { return _vinNumber; }
            private set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Vin Number cannot be blank");
                _vinNumber = value;
            }
        }
        public string CarMake
        {
            get
            { return _carMake; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Car Make cannot be blank");
                _carMake = value;
            }
        }
        public CarType CarTypeN
        {
            get { return _carType; }
            set { _carType = value; }
        }
        public float PurchasePrice
        {
            get  { return _purchasePrice; }
            set
            {
                if(value<=0)
                {
                    throw new Exception("Purchase Price must be greater than 0");
                }
                _purchasePrice = value;
            }
        }
        public int ModelYear
        {
            get { return _modelYear; }
            set
            {
                _modelYear = value;
            }
        }
        public int Mileage
        {
            get { return _mileage; }
            set
            {
                if (value<=0)
                {
                    throw new Exception("Mileage must be greater than 0");
                }
                _mileage = value;
            }
        }
        public float TotalDepreciation
        {
        get
            {
                return _depreciationAmount;
            }
            set
            {
                value = (float) (((_mileage * 0.9*0.01*0.0001*_purchasePrice)+(2020 - _modelYear) * 0.1 * _purchasePrice));
                _depreciationAmount = value;
            }
            
        }
    }
}
