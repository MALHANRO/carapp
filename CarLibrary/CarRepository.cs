﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLibrary
{
    public class CarRepository
    {
        private List<Car> _cars = new List<Car>();

        Car c1 = new Car("R1319RPJ412134567","Audi",46000.50f,CarType.Convertible,2011,7575);
        Car c2 = new Car("P7306T16Q3I88G688","BMW",69000.100f,CarType.Sedan,2011,68000);
        Car c3 = new Car("Q456R6576585J0921","Volkswagen",58000.250f,CarType.Hatchback,2017,4500);

        public void AddCar(Car c)
        {
            if (GetByVin(c.VinNumber) != null)
            {
                throw new Exception("Car with same Vin Number already exists");
            }
            _cars.Add(c);
        }
        public Car GetByVin(string vinNumber)
        {
            foreach (Car c in _cars)
            {
                if (c.VinNumber == vinNumber)
                {
                    return c;
                }
            }
            return null;
        }
    }
}
