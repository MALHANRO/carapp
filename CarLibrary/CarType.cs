﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLibrary
{
    public enum CarType
    {
        Sedan,
        Hatchback,
        Coupe,
        Crossover,
        Convertible,
    }
}
